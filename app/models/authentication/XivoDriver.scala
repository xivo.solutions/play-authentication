package models.authentication

import configuration.AuthConfig
import models.{AuthenticatedToken, AuthenticatedUser, CredentialsValidation, InvalidCredentials}
import play.api.http.Status.NO_CONTENT
import play.api.libs.json.Json
import play.api.libs.ws._

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.{Failure, Success, Try}

case class LoginResult()

class XivoDriver(authConfig: AuthConfig, wsClient: WSClient) extends AuthenticationDriver {
  val realDriver = new RealXivoDriver(new RestapiConnector(authConfig.usersWsPath.get, authConfig.usersWsToken, wsClient))

  override def authenticate(login: String, password: String): Option[CredentialsValidation] = realDriver.authenticate(login, password)
}

protected class RealXivoDriver(val connector: RestapiConnector) extends AuthenticationDriver {
  override def authenticate(login: String, password: String): Option[CredentialsValidation] = {
    connector.validateUser(login: String, password: String) match {
      case Success(AuthenticatedToken) =>
        logger.info(s"Login successful for $login")
        Some(AuthenticatedToken)
      case Success(res: AuthenticatedUser) =>
        logger.info(s"Login successful for $login")
        Some(res)
      case Success(InvalidCredentials) =>
        logger.info(s"Login failed for $login")
        None
      case Failure(t) =>
        logger.error("RestApi error", t)
        None
    }
  }
}

protected class RestapiConnector(usersUrl: String, authToken: Option[String], wsClient: WSClient) {

  val noTokenException = new Exception("Unable to connect to api, token is missing.")

  def validateUser(login: String, password: String): Try[CredentialsValidation] = {
    Try(processRequest(login, password, authToken))
      .map(_.status)
      .map{
        case NO_CONTENT => AuthenticatedUser(login)
        case _ => InvalidCredentials
      }
  }

  def processRequest(login: String, password: String, authToken: Option[String]): WSResponse = {
    val authData = Json.obj("username" -> login, "password" -> password)
    authToken match {
      case Some(token) =>
        Await.result(wsClient.url(usersUrl).withHttpHeaders("X-Auth-Token" -> token).post(authData), 5.second)
      case None => throw noTokenException
    }
  }
}
