package models.authentication

import com.unboundid.ldap.sdk.{LDAPConnection, LDAPException, SearchResult, SearchScope}
import com.unboundid.util.ssl.SSLUtil
import configuration.AuthConfig
import models.AuthenticatedUser

import java.net.URI

class LdapDriver(authConfig: AuthConfig) extends AuthenticationDriver {
  val ldapUri = new URI(authConfig.ldapUri.get)
  var ldapConnection: Option[LDAPConnection] = None

  override def authenticate(login: String, password: String): Option[AuthenticatedUser] = {
    ldapConnection = openLdapConnection(authConfig.ldapManagerDN.get, authConfig.ldapManagerPassword.get)
    val entries = searchEntryWithLogin(login, ldapConnection)
    this.closeLdapConnection(ldapConnection)
    if (entries.isEmpty ||  (entries.isDefined && entries.get.getEntryCount == 0)) {
      logger.warn(s"Incorrect login $login")
      None
    }
    else {
      try {
        val loginDN = entries.get.getSearchEntries.get(0).getDN
        ldapConnection = openLdapConnection(loginDN, password, mustThrowException = false)
        if (ldapConnection.isEmpty) {
          logger.warn(s"Login failed for user $login")
          return None
        }
        Some(AuthenticatedUser(login))
      } catch {
        case e: LDAPException => logger.warn(s"Login failed for user $login")
          this.closeLdapConnection(ldapConnection)
          None
      } finally {
        this.closeLdapConnection(ldapConnection)
      }
    }
  }

  private def closeLdapConnection(connection: Option[LDAPConnection]): Unit = {
    if (connection.isDefined && connection.get.isConnected) {
      connection.get.close()
    }
  }

  private def searchEntryWithLogin(login: String, connection: Option[LDAPConnection]): Option[SearchResult] = {
    if (connection.isDefined) {
      Option (connection.get.search(authConfig.ldapSearchBase.get, SearchScope.SUB,
        s"(${authConfig.ldapSearchFilter.get})".format(login)))
    } else {
      None
    }
  }

  private def openLdapConnection(loginDN: String, password: String, mustThrowException: Boolean = true): Option[LDAPConnection] = {
    try {
      if (ldapUri.getScheme == "ldaps") {
        val sslUtil = new SSLUtil()
        Option(new LDAPConnection(sslUtil.createSSLSocketFactory, ldapUri.getHost, ldapUri.getPort, loginDN,
          password))
      } else {
        Option (new LDAPConnection(ldapUri.getHost, ldapUri.getPort, loginDN, password))
      }
    } catch {
      case e: LDAPException => {
        this.closeLdapConnection(ldapConnection)
        if (mustThrowException) {
          throw new AuthenticationDriverException("LdapDriver", e)
        }
        None
      }
      case er: Exception => {
        this.closeLdapConnection(ldapConnection)
        throw new Exception(er)
      }
    }
  }

}
