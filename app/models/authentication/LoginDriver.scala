package models.authentication

import models.AuthenticatedUser

class LoginDriver(expectedLogin: String, expectedPassword: String) extends AuthenticationDriver {

  override def authenticate(login: String, password: String): Option[AuthenticatedUser] = {
    if (login.equals(expectedLogin) && password.equals(expectedPassword)) {
      logger.info(s"Login successful for $login")
      Some(AuthenticatedUser(login, superAdmin = true))
    }
    else {
      logger.warn(s"Echec d'authentification pour l'utilisateur $login")
      None
    }
  }
}
