package models.authentication

import com.google.inject.ImplementedBy
import models.{AuthenticatedUser, CredentialsValidation}
import configuration.AuthConfig

import javax.inject.{Inject, Singleton}
import play.api.libs.ws.WSClient


@ImplementedBy(classOf[AuthenticationProviderImpl])
trait AuthenticationProvider {
  type AuthenticationFunction = (String,String) => Option[CredentialsValidation]

  val authenticationFunctions:List[AuthenticationFunction]

  private def firstAuthentOk(username:String, password: String) = {
    authenticationFunctions.view.map(configAuthent => configAuthent(username, password)).find(_.isDefined).flatten
  }

  def authenticate(username:String, password: String):Option[CredentialsValidation] = {
    firstAuthentOk(username, password)
  }

}

@Singleton
class AuthenticationProviderImpl @Inject()(authConfig: AuthConfig, wsClient: WSClient) extends AuthenticationProvider {
  val config = authConfig
  val ldap: Option[AuthenticationDriver] = getLdapDriver(config)
  val xivo: Option[AuthenticationDriver] = getXivoDriver(config, wsClient)
  val login: Option[AuthenticationDriver] = getLoginDriver(config)

  def getLdapDriver(config: AuthConfig): Option[AuthenticationDriver] = {
    config.ldapUri.flatMap(uri => Some(new LdapDriver(config)))
  }

  def getXivoDriver(config: AuthConfig, wsClient: WSClient): Option[AuthenticationDriver] = {
    config.usersWsPath.flatMap(path => Some(new XivoDriver(config, wsClient)))
  }

  def getLoginDriver(config: AuthConfig): Option[AuthenticationDriver] =
    for {
      expectedLogin <- config.expectedLogin
      expectedPassword <- config.expectedPassword
    } yield new LoginDriver(expectedLogin, expectedPassword)

  override val authenticationFunctions:List[AuthenticationFunction] = {
    List[Option[AuthenticationFunction]](
      xivo.map(xivo=>xivo.authenticate),
      login.map(login=>login.authenticate),
      ldap.map(ldap=>ldap.authenticate)
    ).flatten
  }
}
