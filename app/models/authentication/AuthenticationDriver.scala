package models.authentication

import models.CredentialsValidation
import play.api.Logger

abstract class AuthenticationDriver {
  val logger: Logger = Logger(getClass.getName)

  def authenticate(login: String, password: String): Option[CredentialsValidation]

}
