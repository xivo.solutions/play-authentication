package models.authentication

class AuthenticationDriverException(driver: String, originalException: Exception)
  extends Exception(s"Unexpected authentication problem in $driver module, error: ${originalException.getMessage}")

