package models

import models.authentication.AuthenticationProvider

sealed trait CredentialsValidation
case class AuthenticatedUser(username: String, superAdmin: Boolean=false) extends CredentialsValidation
case object AuthenticatedToken extends CredentialsValidation
case object InvalidCredentials extends CredentialsValidation

class AuthenticateUser(authenticationProvider: AuthenticationProvider) {
  def authenticate(username: String, password: String): Option[CredentialsValidation] = authenticationProvider.authenticate(username, password)
}


