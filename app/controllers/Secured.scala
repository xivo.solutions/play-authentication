package controllers

import com.google.inject.Inject
import configuration.AuthConfig
import models.{AuthenticatedToken, AuthenticatedUser, CredentialsValidation}
import play.api.mvc._
import xivo.service._

import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success}
import scala.concurrent.duration._

class Secured @Inject() (authConfig: AuthConfig, xivoWs: XivoWebService, action: DefaultActionBuilder) {
  private def username(request: RequestHeader): Option[CredentialsValidation] = {
    request.session.get("username") match {
      case Some(username) => Some(AuthenticatedUser(username, request.session.get("isSuperAdmin").contains("true")))
      case None => request.headers.get("X-Auth-Token") match {
        case Some(value) if authConfig.token.contains(value) => Some(AuthenticatedToken)
        case _ => None
      }
    }
  }

  private def authFromWebi(request: RequestHeader): Option[AuthenticatedUser] = {
    request.cookies.get("_eid") match {
      case Some(cookie) =>
        Await.ready(xivoWs.validateCookieAgainstXiVO(cookie), 1200.millis).value.get match {
          case Success(XivoCookieResponse) if authConfig.expectedLogin.isDefined => Some(AuthenticatedUser(authConfig.expectedLogin.get, false))
          case Success(XivoCookieResponse) => None
          case Failure(e) => None
        }
      case None => None
      }
  }

  private def allAuthenticationMethod(request: RequestHeader): Option[CredentialsValidation] =
    username(request) orElse authFromWebi(request)

  private def onUnauthorized(loginRoute: Option[Call], request: RequestHeader) = {
    loginRoute match {
      case Some(route) => Results.Redirect(route)
      case _ => Results.Forbidden
    }
  }

  def IsAuthenticated(loginRoute: Option[Call], f: => Request[AnyContent] => Result): EssentialAction = Security.Authenticated(username, onUnauthorized(loginRoute, _)) { _ =>
    action(request => f(request))
  }

  def WithAuthenticatedUser(loginRoute: Option[Call], f: => CredentialsValidation => Request[AnyContent] => Result): EssentialAction =
    Security.Authenticated(allAuthenticationMethod, onUnauthorized(loginRoute, _)) {
      user => action(request => f(user)(request))
    }

  def WithAuthenticatedUserAsync(loginRoute: Option[Call], f: => CredentialsValidation => Request[AnyContent] => Future[Result]): EssentialAction =
    Security.Authenticated(allAuthenticationMethod, onUnauthorized(loginRoute, _)) {
      user => action.async { request => f(user)(request) }
    }
}
