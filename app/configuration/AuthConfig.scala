package configuration

import javax.inject.{Inject, Singleton}
import scala.collection.mutable.Buffer
import scala.jdk.CollectionConverters._

@Singleton
class AuthConfig @Inject()(configuration: play.api.Configuration) {
  val config = configuration

  val usersWsPath: Option[String] = getConfStringOpt("authentication.xivo.restapi.users")
  val usersWsToken: Option[String] = getConfStringOpt("authentication.xivo.restapi.token")
  val ldapUri: Option[String] = getConfStringOpt("authentication.ldap.url")
  val ldapManagerDN: Option[String] = getConfStringOpt("authentication.ldap.managerDN")
  val ldapManagerPassword: Option[String] = getConfStringOpt("authentication.ldap.managerPassword")
  val ldapSearchBase: Option[String] = getConfStringOpt("authentication.ldap.searchBase")
  val ldapSearchFilter: Option[String] = getConfStringOpt("authentication.ldap.userSearchFilter")
  val expectedLogin: Option[String] = getConfStringOpt("authentication.login.login").filter(_.nonEmpty)
  val expectedPassword: Option[String] = getConfStringOpt("authentication.login.password").filter(_.nonEmpty)
  val token: Option[String] = getConfStringOpt("authentication.token")

  private def getConfString(path: String): String = {
    configuration.underlying.getString(path)
  }

  private def getConfStringOpt(path: String): Option[String] = {
    if (configuration.keys.contains(path) || configuration.subKeys.contains(path)) {
      Some(configuration.underlying.getString(path))
    } else { None }
  }

  private def getConfStringList(path: String): Buffer[String] = {
    configuration.underlying.getStringList(path).asScala
  }
}
