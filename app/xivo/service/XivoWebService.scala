package xivo.service

import com.google.inject.{ImplementedBy, Inject}
import play.api.Configuration
import play.api.libs.ws._
import play.api.mvc._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

@ImplementedBy(classOf[XivoWebServiceConfigurationImpl])
trait XivoWebServiceConfiguration {
  def getXivoHost: Option[String]
}

class XivoWebServiceConfigurationImpl @Inject() (configuration: Configuration) extends XivoWebServiceConfiguration {
  override def getXivoHost: Option[String] =
    configuration.getOptional[String]("xivohost")
}

case object XivoCookieResponse
class ForbiddenException extends Exception("This cookie is not valid.")
case class XivoCookieException(message: String) extends Exception(message: String)

class XivoWebService @Inject() (ws: WSClient, config: XivoWebServiceConfiguration) {

  def validateCookieAgainstXiVO(cookie: Cookie): Future[XivoCookieResponse.type] = {
    val xivohost = config.getXivoHost match {
      case Some(host) => Future.successful(host)
      case None => Future.failed(new Exception("xivohost parameter is not set in configuration file"))
    }
    xivohost.flatMap(host => {
      ws.url(s"https://${host}/service/ipbx/auth.php/control_system/authenticate/")
        .addCookies(DefaultWSCookie(cookie.name, cookie.value))
        .get().map { response =>
          if (response.status == 200) XivoCookieResponse
          else if (response.status == 403) throw new ForbiddenException
          else throw XivoCookieException(s"Request to XiVO cookie authentication failed with status ${response.status} and message ${response.body}")
        }})
  }
}

