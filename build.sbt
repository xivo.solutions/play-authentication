val appName = "play-authentication"
val appVersion = "2023.11.01-play3"
val appOrganisation = "solutions.xivo"

lazy val main = Project(appName, file("."))
  .enablePlugins(PlayScala)
  .settings(
    name := appName,
    organization := appOrganisation,
    version := appVersion,
    resolvers ++= Dependencies.resolutionRepos,
    libraryDependencies ++= Dependencies.runDep ++ Dependencies.testDep,
    scalaVersion := Versions.defaultScalaVersion
  )
  .settings(sonatypeSettings: _*)
  .settings(productSettings: _*)


lazy val sonatypeSettings = Seq(
  pomIncludeRepository := { _ => false },
  publishMavenStyle := true,
  publishTo := {
    val nexus = "https://oss.sonatype.org/"
    if (isSnapshot.value)
      Some("snapshots" at nexus + "content/repositories/snapshots")
    else
      Some("releases"  at nexus + "service/local/staging/deploy/maven2")
  },
  Test / publishArtifact := false
)

lazy val productSettings = Seq(
  licenses := Seq("LGPL" -> url("https://www.gnu.org/licenses/lgpl.html")),
  homepage := Some(url("https://www.xivo.solutions/")),
  scmInfo := Some(
    ScmInfo(
      url("https://gitlab.com/xivoxc/play-authentication"),
      "scm:git@gitlab.com:xivoxc/play-authentication.git"
    )
  ),
  developers := List(
    Developer(
      id = "randd",
      name = "R&D",
      email = "randd@xivo.solutions",
      url = url("https://xivo.solutions")
    )
  )
)
