package models.authentication

import configuration.AuthConfig
import models.AuthenticatedUser
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.test.PlaySpecification
import play.api.Application

class LoginDriverSpec extends PlaySpecification {

  val app: Application = new GuiceApplicationBuilder()
    .configure(TestConfig.testConfig)
    .build()

  val authConfig = new AuthConfig(app.configuration)

  "The LoginDriver" should {
    "return a User if the login and the password are correct" in {
      running(app) {
        val driver = new LoginDriver(authConfig.expectedLogin.get, authConfig.expectedPassword.get)
        driver.authenticate("john", "doe") shouldEqual Some(AuthenticatedUser("john", true))
      }
    }

    "return None otherwise" in {
      running(app) {
        val driver = new LoginDriver(authConfig.expectedLogin.get, authConfig.expectedPassword.get)
        driver.authenticate("john", "lennon") shouldEqual None
      }
    }
  }
}
