package models.authentication

import java.net.InetAddress
import com.unboundid.ldap.listener.{InMemoryDirectoryServer, InMemoryDirectoryServerConfig, InMemoryListenerConfig}
import com.unboundid.ldap.sdk.Attribute
import configuration.AuthConfig
import models.AuthenticatedUser
import play.api.test.PlaySpecification
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.Application
import models.authentication.TestConfig
import org.scalatestplus.mockito.MockitoSugar
import org.specs2.matcher.MatchResult

class LdapDriverSpec extends PlaySpecification with MockitoSugar {

  "The LdapDriver" should {

    var helperServer: Option[InMemoryDirectoryServer] = None
    val app = new GuiceApplicationBuilder()
      .configure(TestConfig.testConfig)
      .build()

    step({
      val cfg = new InMemoryDirectoryServerConfig(TestConfig.searchBase)
      cfg.addAdditionalBindCredentials(TestConfig.managerDN, TestConfig.managerPassword)
      cfg.addAdditionalBindCredentials("cn=John Doe,ou=people1,o=test", "nobody")
      cfg.addAdditionalBindCredentials("cn=Buck Danny,ou=people2,o=test", "nobody")
      cfg.setListenerConfigs(InMemoryListenerConfig.createLDAPConfig("LDAP", InetAddress.getByName("localhost"), TestConfig.port, null));
      val server = new InMemoryDirectoryServer(cfg)
      server.startListening()
      server.add("o=test", new Attribute("objectClass", "organization"))
      server.add("ou=people1,o=test", new Attribute("objectClass", "organizationalUnit"))
      server.add("ou=people2,o=test", new Attribute("objectClass", "organizationalUnit"))
      server.add("cn=John Doe,ou=people1,o=test", new Attribute("objectClass", "person"),
        new Attribute("objectClass", "iNetOrgPerson"), new Attribute("sn", "John"), new Attribute("mail", "j.doe@test.com"))
      server.add("cn=Buck Danny,ou=people2,o=test", new Attribute("objectClass", "person"),
        new Attribute("objectClass", "iNetOrgPerson"), new Attribute("sn", "Buck"), new Attribute("mail", "b.danny@test.com"))

      helperServer = Some(server)
      ok("server is up")
    })

    "return a User if the login and the password are correct" in {
      running(app) {
        val driver = new LdapDriver(new AuthConfig(app.configuration))
        driver.authenticate("j.doe@test.com", "nobody") shouldEqual Some(AuthenticatedUser("j.doe@test.com"))
        driver.authenticate("b.danny@test.com", "nobody") shouldEqual Some(AuthenticatedUser("b.danny@test.com"))
      }
    }

    "return None if the username is incorrect" in {
      running(app) {
        val driver = new LdapDriver(new AuthConfig(app.configuration))
        driver.authenticate("j.wrong", "nobody") shouldEqual None
      }
    }

    "return False if the ldap connection is closed correctly" in {
      running(app) {
        val driver = new LdapDriver(new AuthConfig(app.configuration))
        driver.authenticate("j.wrong", "nobody")
        driver.ldapConnection.get.isConnected shouldEqual false
      }
    }

    "return None if the password is incorrect" in {
      running(app) {
        val driver = new LdapDriver(new AuthConfig(app.configuration))
        driver.authenticate("j.doe@test.com", "wrong") shouldEqual None
      }
    }

    "throw an exception if the ldap server is down" in {
      helperServer.get.shutDown(true)
      ok("server down")
      running(app) {
        val driver = new LdapDriver(new AuthConfig(app.configuration))
        driver.authenticate("j.doe", "somebody") must throwAn[AuthenticationDriverException]
      }
    }

  }
}
