package models.authentication

import configuration.AuthConfig
import models.AuthenticatedUser
import org.mockito.Mockito.{times, verify, when}
import org.scalatestplus.mockito.MockitoSugar
import play.api.inject.guice.GuiceApplicationBuilder
import org.scalatest.flatspec.AnyFlatSpec
import org.specs2.concurrent.ExecutionEnv
import play.api.Application
import play.api.test.PlaySpecification

class AuthenticationProviderSpec(implicit ee: ExecutionEnv) extends PlaySpecification with MockitoSugar {

  val authDriver: AuthenticationDriver = mock[AuthenticationDriver]
  val unconfiguredDriver: AuthenticationDriver = mock[AuthenticationDriver]
  val authSecondDriver: AuthenticationDriver = mock[AuthenticationDriver]
  val authOtherDriver: AuthenticationDriver = mock[AuthenticationDriver]

  val port = 5000
  val AuthentConfig: Map[String,String] = Map(
    "authentication.ldap.url" -> s"ldap://localhost:$port",
    "authentication.ldap.loginChain" -> "cn=%s,o=test",
    "authentication.login.login" -> "john",
    "authentication.login.password" -> "doe",
    "authentication.driver.test" -> "testProperty",
    "authentication.otherdriver.test" -> "testProperty",
    "authentication.xivo.restapi.users" -> "",
    "authentication.ldap.managerDN" -> "",
    "authentication.ldap.managerPassword" -> "",
    "authentication.ldap.searchBase" -> "",
    "authentication.ldap.userSearchFilter" -> "",
    "authentication.token" -> ""
  )
  val app: Application = new GuiceApplicationBuilder()
    .configure(AuthentConfig)
    .build()

  object TestAuthentProvider extends AuthenticationProvider {
    override val authenticationFunctions:List[AuthenticationFunction]= List(authDriver.authenticate)
    val config = new AuthConfig(app.configuration)
  }

  object TestUnconfiguredAuthentProvider extends AuthenticationProvider {
    override val authenticationFunctions:List[AuthenticationFunction]= List()
    val config = new AuthConfig(app.configuration)
  }

  object TestAllDrivers extends AuthenticationProvider {
    override val authenticationFunctions:List[AuthenticationFunction]= List(authSecondDriver.authenticate,authOtherDriver.authenticate)
    val config = new AuthConfig(app.configuration)
  }

  "The AuthenticationProvider" should {

    "should call authent driver" in {
      val uname = Some(AuthenticatedUser("username"))
      when(authDriver.authenticate("username","password")).thenReturn(uname)

      running(app) {
        TestAuthentProvider.authenticate("username", "password") shouldEqual(uname)
      }

      verify(authDriver).authenticate("username","password")
      ok("Test done")
    }

    "should not call any unconfigured driver" in {
      running(app) {
        TestUnconfiguredAuthentProvider.authenticate("username", "password")
      }

      verify(unconfiguredDriver, times(0)).authenticate("username", "password")
      ok("Test done")
    }

    "should stop after calling first driver which send back a user" in {
      val uname = Some(AuthenticatedUser("username"))
      when(authDriver.authenticate("username", "password")).thenReturn(uname)


      when(authSecondDriver.authenticate("username","password")).thenReturn(Some(AuthenticatedUser("username")))
      when(authOtherDriver.authenticate("username","password")).thenReturn(None)

      running(app) {
        TestAllDrivers.authenticate("username", "password")
      }

      verify(authSecondDriver).authenticate("username", "password")
      verify(authOtherDriver, times(0)).authenticate("username", "password")
      ok("Test done")
    }

  }

}
