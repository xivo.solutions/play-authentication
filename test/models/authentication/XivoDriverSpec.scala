package models.authentication

import models.{AuthenticatedUser, InvalidCredentials}
import org.mockito.Mockito.{times, verify, when}
import org.scalatestplus.mockito.MockitoSugar
import org.specs2.concurrent.ExecutionEnv
import play.api.Application
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.libs.ws.{WSClient, WSRequest, WSResponse}
import play.api.test.PlaySpecification
import play.api.libs.ws.writeableOf_JsValue

import scala.concurrent.Future
import scala.util.{Failure, Success}

class XivoDriverSpec(implicit ee: ExecutionEnv) extends PlaySpecification with MockitoSugar {

  private def getApp(cfg: Option[Map[String, String]] = None): Application = {
    val builder = new GuiceApplicationBuilder()
    cfg.foreach(builder.configure(_))
    builder.build()
  }

  "The XivoDriver" should {

    "return a User if the login and the password are correct" in {
      running(getApp(Some(TestConfig.testConfig))) {
        val url = "http://someUrl/"
        val wsClient = mock[WSClient]
        val wsRequest = mock[WSRequest]
        val wsResponse = mock[WSResponse]
        when(wsResponse.status).thenReturn(204)
        val authData = Json.obj("username" -> "john", "password" -> "doe")
        when(wsClient.url(url)).thenReturn(wsRequest)
        when(wsClient.url(url).withHttpHeaders("X-Auth-Token" -> "abcd-efgh")).thenReturn(wsRequest)
        when(wsClient.url(url).withHttpHeaders("X-Auth-Token" -> "abcd-efgh").post(authData))
          .thenReturn(Future.successful(wsResponse))

        val token = Some("abcd-efgh")
        val connector = new RestapiConnector(url, token, wsClient)
        val driver = new RealXivoDriver(connector)
        connector.validateUser("john", "doe") shouldEqual Success(AuthenticatedUser("john"))
        driver.authenticate("john", "doe") shouldEqual Some(AuthenticatedUser("john"))
      }
    }

    "not return a User if the login and the password are incorrect" in {
      running(getApp(Some(TestConfig.testConfig))) {
        val url = "http://someUrl/"
        val wsClient = mock[WSClient]
        val wsRequest = mock[WSRequest]
        val wsResponse = mock[WSResponse]
        when(wsResponse.status).thenReturn(401)
        val authData = Json.obj("username" -> "john", "password" -> "doe")
        when(wsClient.url(url)).thenReturn(wsRequest)
        when(wsClient.url(url).withHttpHeaders("X-Auth-Token" -> "abcd-efgh")).thenReturn(wsRequest)
        when(wsClient.url(url).withHttpHeaders("X-Auth-Token" -> "abcd-efgh").post(authData))
          .thenReturn(Future.successful(wsResponse))

        val token = Some("abcd-efgh")
        val connector = new RestapiConnector(url, token, wsClient)
        val driver = new RealXivoDriver(connector)
        connector.validateUser("john", "doe") shouldEqual Success(InvalidCredentials)
        driver.authenticate("john", "doe") shouldEqual None
      }
    }

    "return a response if there is auth token" in {
      running(getApp(Some(TestConfig.testConfig))) {
        val wsClient = mock[WSClient]
        val url = "http://someUrl/"
        val token = Some("abcd-efgh")
        val connector = new RestapiConnector(url, token, wsClient)
        val wsResponse = mock[WSResponse]
        val wsRequest = mock[WSRequest]
        val authData = Json.obj("username" -> "john", "password" -> "doe")

        when(wsClient.url(url)).thenReturn(wsRequest)
        when(wsClient.url(url).withHttpHeaders("X-Auth-Token" -> "abcd-efgh")).thenReturn(wsRequest)
        when(wsClient.url(url).withHttpHeaders("X-Auth-Token" -> "abcd-efgh").post(authData))
          .thenReturn(Future.successful(wsResponse))

        connector.processRequest("john", "doe", token) shouldEqual wsResponse

        verify(wsClient.url(url).withHttpHeaders("X-Auth-Token" -> "abcd-efgh"), times(1)).post(authData)
        ok("Test done")
      }
    }

    "return invalid credentials if the response is not success" in {
      running(getApp(Some(TestConfig.testConfig))) {
        val wsClient = mock[WSClient]
        val url = "http://someUrl/"
        val token = Some("abcd-efgh")
        val connector = new RestapiConnector(url, token, wsClient)
        val wsResponse = mock[WSResponse]
        val wsRequest = mock[WSRequest]
        val authData = Json.obj("username" -> "john", "password" -> "doe")

        when(wsClient.url(url)).thenReturn(wsRequest)
        when(wsClient.url(url).withHttpHeaders("X-Auth-Token" -> "abcd-efgh")).thenReturn(wsRequest)
        when(wsClient.url(url).withHttpHeaders("X-Auth-Token" -> "abcd-efgh").post(authData))
          .thenReturn(Future.successful(wsResponse))
        when(wsResponse.status)
          .thenReturn(NOT_FOUND)

        connector.processRequest("john", "doe", token) shouldEqual wsResponse
        connector.validateUser("john", "doe") shouldEqual Success(InvalidCredentials)
      }
    }

    "return None if token is not provided" in {
      running(getApp(Some(TestConfig.testConfig))) {
        val wsClient = mock[WSClient]
        val url = "http://someUrl/"
        val connector = new RestapiConnector(url, None, wsClient)
        val driver = new RealXivoDriver(connector)
        connector.validateUser("john", "lennon") shouldEqual Failure(connector.noTokenException)
        driver.authenticate("john", "lennon") shouldEqual None
      }
    }

    "throw an exception if exception occurs during request processing" in {
      running(getApp()) {
        val url = "http://someUrl/"
        val token = None
        val wsClient = mock[WSClient]
        val connector = new RestapiConnector(url, token, wsClient)
        val driver = new RealXivoDriver(connector)

        connector.processRequest("john", "doe", token) must throwAn[Exception]
        driver.authenticate("john", "lennon") shouldEqual None
      }
    }
  }
}
