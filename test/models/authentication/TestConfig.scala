package models.authentication

object TestConfig {

  val port = 5000
  val searchBase = "o=test"
  val managerDN = "cn=admin,o=test"
  val managerPassword = "superpass"

  val testConfig: Map[String,String] = Map(
    "authentication.ldap.url" -> s"ldap://localhost:$port",
    "authentication.ldap.searchBase" -> searchBase,
    "authentication.ldap.managerDN" -> managerDN,
    "authentication.ldap.managerPassword" -> managerPassword,
    "authentication.ldap.userSearchFilter" -> "mail=%s",
    "authentication.login.login" -> "john",
    "authentication.login.password" -> "doe"
  )

}
