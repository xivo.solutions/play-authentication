# XivoCC Play Authentication

 Web authentication library for XivoCC

![scala steward](https://img.shields.io/badge/Scala_Steward-helping-light.svg)
![pipeline](https://gitlab.com/xivo.solutions/play-authentication/badges/master/pipeline.svg?ignore_skipped=true)

![scalaversion](https://img.shields.io/badge/Scala%203-DC322F)
![play](https://img.shields.io/badge/Play%20Framework-92d13d)

## Test

    $ sbt clean test

## Branches

* TODO tag master in 2.8

* Master : Play 2.8 for all applications
* Play-2.6 : for config-mgt on scala 2.11 (for xuc, as was using scala 2.12, use tag play2.6-2018.03.01 on master branch)
* Play-2.5 : for Play 2.5
* Play-2.4 : for Play 2.4

## Release notes

**2023.11.00-play3 - 2023/11/13**
- Move to play 3
- Move to scala 3.3
- Fixed all specs

**2020.20.00-play2.8 - 2020/12/04**
- Move to play 2.8
- reuse one single version for all play application

**2018.15.01-play2.6 - 2020/12/04**
- merge of play-2.6 branch but works for scala 2.12 only (never used, only for merge purpose)
- add webi authentication by cookie

**2018.15.00-play2.6 - 2018/10/08**
- Only exists on play-2.6 branch for scala 2.11 only (and so used in config-mgt until freya included)
- add webi authentication by cookie

**play2.6-2018.03.02-SNAPSHOT - 2018/05/31**
- Move to play auth 2.6
- maven central publication

**2018.03.00-play2.5 - 2018/02/13**
- Move to play auth 2.5
- Allow to connect through xivo auth token

**2.4 - 2016/04/19**
- Integrates PlayFW DI
- To be more accurate - all the configured authentication methods are tried, on the first success the authentication is validated
- method key has been removed as it was confusing and without any impact

**1.9 - 2015/04/09**
- More complex but more flexible way to configure the LDAP Driver :
<pre>
authentication {
    ldap {
        managerDN = "cn=admin,o=myorg"     # user with read rights on the whole LDAP
        managerPassword = "superpass"      # password for this user
        ldapUri = "ldap://10.0.0.2:789"    # ldap URI
        searchBase = "ou=people,o=test"    # ldap entry to use as search base
        userSearchFilter = "uid=%s"        # filter to use to search users by login, using a string pattern
    }
}
</pre>

**1.7 - 2015/04/07**
- LdapDriver now supports multiple base DN. So loginChain is renamed to loginChains and is a list :
<pre>
authentication {
    method = "..."
    ldap {
        loginChains = ["...", "..."]
        ...
    }
}
</pre>

**1.5 - 2014/10/22**
- Exceptions in XiVO and LDAP drivers are translated to the parent application to allow detailed error messages

**1.4 - 2014/10/21**
- refactor configuration, all keys are in authentication node as follows:
<pre>
authentication {
    method = "..."
    login {
        login = "..."
        password = "..."
    }
}
</pre>

## Versioning

play2.6-2018.03.01-SNAPSHOT

Starts by the play version supported, followed by the xivo version used when changing the library

Add SNAPSHOT for dev version

## Publication to maven central

Published here : https://oss.sonatype.org/content/repositories/snapshots/solutions/xivo/
Key used : http://pool.sks-keyservers.net/pks/lookup?op=vindex&fingerprint=on&search=0x6D69664BAA111663

Uses [sbt-sonatype](https://github.com/xerial/sbt-sonatype) plugin and [sbt-pgp](https://www.scala-sbt.org/sbt-pgp/index.html) plugin

* Does not support passphrase.
* Key is deployed on Jenkins, but the job is not working

Configure gbg plugin in your home directory 
   
    home/.sbt/0.13/plugins/gbg.sbt contains addSbtPlugin("com.jsuereth" % "sbt-pgp" % "2.0.1")

Public and secret key should be deployed on your local key directory with no passphrase :

    $ gpg --list-public-keys

    ---------------------------------
    pub   rsa2048 2018-05-28 [SC]
          28C944F64E24254FE72881706D69664BAA111663
    uid           [ultimate] XiVO Solutions <jylebleu@xivo.solutions>
    sub   rsa2048 2018-05-28 [E]
    
    $gpg --list-secret-keys
    
    ---------------------------------
    sec   rsa2048 2018-05-28 [SC]
          28C944F64E24254FE72881706D69664BAA111663
    uid           [ultimate] XiVO Solutions <jylebleu@xivo.solutions>
    ssb   rsa2048 2018-05-28 [E]
    
### Process ###

- Publish the signed jars -> sbt clean test publishSigned
- Log into nexus repository manager with xivo : https://oss.sonatype.org/
- Follow the releasing process : https://central.sonatype.org/pages/releasing-the-deployment.html


## License


This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

See the [COPYING](COPYING) and [COPYING.LESSER](COPYING.LESSER) files for details.
