import play.sbt.PlayImport._
import sbt.{ModuleID, _}

object Versions {
  val play = "3.0.0"
  val defaultScalaVersion = "3.4.1"
  val scalatestplay      = "7.0.1"
  val mockito             = "5.11.0"
}

object Library {
  val mockito = "org.mockito" % "mockito-core" % Versions.mockito
  val scalaTest  = "org.scalatest" %% "scalatest"      % "3.2.17"
  val ldapsdk = "com.unboundid" % "unboundid-ldapsdk" % "7.0.0"
  val scalatestplay =
    "org.scalatestplus.play" %% "scalatestplus-play" % Versions.scalatestplay
  val scalatestmock = "org.scalatestplus" %% "mockito-3-4" % "3.2.10.0"
}

object Dependencies {

  val resolutionRepos = Seq(
    ("Local Maven Repository" at "file:///" + Path.userHome.absolutePath + "/.m2/repository"),
    ("scalaz-bintray" at "https://dl.bintray.com/scalaz/releases")
  )

  import Library._

  val runDep = run(
    ws,
    ldapsdk,
    guice
  )

  val testDep = test(
    specs2,
    mockito,
    scalatestplay,
    scalatestmock
  )

  def run       (deps: ModuleID*): Seq[ModuleID] = deps
  def test      (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "test")
}